import sys
from BotBase.Reactor import ftypes
from BotBase.BotTools import secondsUntilNext
from main import admin, modules, conf

def addCallFuncs(mod, funcs, admin):
	for i in funcs:
		dbconn.callDBFunc("addcallfunction", [mod, i])
		if (funcs[i]):
			for user in admin:
				dbconn.callDBFunc("limitfunc", [user, i])

def addSpecFuncs(mod, specs):
	for i in specs:
		if i not in ftypes:
			continue
		for j in specs[i]:
			dbconn.callDBFunc("addspecialfunction", [mod, j, i])

def addTextMatchers(mod, matchers):
	for i in matchers:
		for j in matchers[i]:
			dbconn.callDBFunc("addtextfunction", [mod, i, j])

def addTimerFuncs(mod, timers):
	for i in timers:
		if ':' in timers[i]:
			times = list()
			for t in timers[i].split(','):
				if secondsUntilNext(t) != None:
					times.append(t)
			if len(times) == 0:
				continue
			timings = ",".join(times)
		else:
			try:
				int(timers[i])
				timings = timers[i]
			except:
				continue
		dbconn.callDBFunc("addtimerfunction", [mod, i, timings])

def populateDB():
	for i in modules:
		__import__("IdealMods.%s" % (i))

	for user in admin:
		dbconn.callDBFunc("adduser", [user, admin[user]])

	mods = dir(sys.modules["IdealMods"])
	for i in mods:
		if i[0] != '_':
			mod = sys.modules["IdealMods."+i]
			try:	
				func = getattr(mod, 'getCalls')
				if callable(func):
					calls = func()
					addCallFuncs(i, calls, admin)
			except:
				pass
			try:
				func = getattr(mod, 'getSpecials')
				if callable(func):	
					specs = func()
					addSpecFuncs(i, specs)
			except:
				pass
			try:
				func = getattr(mod, 'textMatchers')
				if (callable(func)):	
					textmatch = func()
					addTextMatchers(i, textmatch)
			except:
				pass
			try:
				func = getattr(mod, 'getTimers')
				if (callable(func)):	
					timers = func()
					addTimerFuncs(i, timers)
			except:
				pass

dbconn = conf.getValue('dbconn')(conf.getValue('dbconfig'))
populateDB()
