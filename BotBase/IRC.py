#coding=UTF-8
import socket, random, imp, sys, traceback, time
from datetime import datetime
from BotBase import Parser, Timerfuncs, Nicklist
from threading import Semaphore, Thread, Timer

ircVersion = '1.0-rc1'

class IRCBot:
	def __init__(self, config):
		self.config = config
		self.reconnectTimeout = config.getValue('reconnect')

		self.sendBuffer = list()
		self.sendSemaph = Semaphore(0)

		self.sender = Sender(self, self.sendSemaph)
		if self.config.getValue('delay') != None:
			self.sender.setDelay(config.getValue('delay'))
		self.sender.start()

		self.connect()
	
	def connect(self):
		self.shutting = False
		self.connected = datetime.now()
		self.nicklist = Nicklist.Nicklist()
		self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.conn.settimeout(self.config.getValue('timeout'))
		self.sender.reset()

		try:
			self.conn.connect( (self.config.getValue('server'), self.config.getValue('port')) )
		except:
			print('Connect failed.')
			self.quit('Connection failure', True)
			return
	
		self.reload()

		self.send( 'NICK '+self.config.getValue('nick') )
		self.send( 'USER '+self.config.getValue('user')+' * * :'+self.config.getValue('realname') )
		if self.config.getValue('operpass') != None:
			self.send ( 'OPER %s %s' % (self.config.getValue('nick'), self.config.getValue('operpass')))

	def send(self, cmd):
		cmd += '\r\n';
		self.sendBuffer.append( cmd.encode() )
		self.sendSemaph.release()
	
	def recv(self, buffsize):
		return self.conn.recv( buffsize )
		
	def msg(self, target, text):
		texts = list()
		if len(text) <= 400:
			texts.append(text)
		else:
			while len(text) > 400:
				i = 400;
				while text[i] != ' ':
					i = i - 1
				texts.append(text[0:i])
				text = text[i+1:]
			texts.append(text)
		for i in texts:
			msg = 'privmsg '+target+' :'+i+'\r\n'
			self.send(msg)

	def kick(self, chan, nick, reason):
		self.send('kick %s %s :%s' % (chan, nick, reason))

	def mode(self, chan, modes):
		self.send('mode %s %s' % (chan, modes))

	def join(self, chan, passwd = ''):
		self.send('join %s %s' % (chan, passwd))
	
	def part(self, chan, reason = 'Leaving'):
		self.send('part %s :%s' % (chan, reason))

	def topic(self, chan, newtopic):
		self.send('topic %s :%s' % (chan, newtopic))
		
	def invite(self, chan, nick):
		self.send('invite %s %s' % (nick, chan))

	def quit(self, reason = 'pyIdealGas shutting down..', reload = False):
		if self.shutting:
			return
		self.shutting = True
		cur = len(self.sendBuffer)
		while len(self.sendBuffer) > 0:
			if len(self.sendBuffer) != cur:
				cur = len(self.sendBuffer)
			pass
		if not reload:
			self.sender.stop()
	
		if 'parser' in dir(self):
			self.parser.stop()

		self.send('quit :%s' % (reason))
		self.sender.join(2)

		if not reload:
			self.conn.shutdow()
			self.conn.close()
		else:
			self.conn.detach()
			tdiff = datetime.now() - self.connected
			if tdiff.seconds < self.reconnectTimeout:
				sleeptime = self.reconnectTimeout - tdiff.seconds
				print('Waiting %d until reconnect' % (sleeptime))
			else:
				sleeptime = 0
			t = Timer(sleeptime, self.connect)
			t.start()

	def getDBConn(self):
		return self.config.getValue('dbconn')(self.config.getValue('dbconfig'))
	
	def getNicklist(self):
		return self.nicklist

	def getConfig(self):
		return self.config

	def getData(self):
		return self.sendBuffer.pop(0)

	def getSocket(self):
		return self.conn

	def reload(self):
		if 'parser' in dir(self):
			self.parser.stop()

		imp.reload(Parser)
		if callable(Parser.onReload):
			Parser.onReload()

		self.parser = Parser.Parse(self)
		self.parser.daemon = True
		self.parser.start()

		Timerfuncs.stopTimers(self)
		imp.reload(Timerfuncs)
		if callable(Timerfuncs.onReload):
			Timerfuncs.onReload()
		Timerfuncs.startTimers(self)

class Sender(Thread):
	def __init__(self, irc, sendSemaph):
		Thread.__init__(self)
		self.bot = irc
		self.ssem = sendSemaph
		self.asem = Semaphore(0)
		self.delay = 1

	def setDelay(self, val):
		try:
			self.delay = float(val)
		except:
			pass

	def reset(self):
		self.broken = False
	
	def run(self):
		self.running = True
		while self.running:
			t = Timer(self.delay, self.asem.release)
			t.start()
			
			self.asem.acquire()
			self.ssem.acquire()
			row = self.bot.getData()
			if self.broken:
				continue
			try:
				self.bot.getSocket().send(row)
			except:
				if self.bot.getConfig().getValue('debug'):
					print('Failed connection while sending: %s' % (row))
				self.broken = True
				self.bot.quit('Broken connection', True)
			
	def stop(self):
		self.running = False	

print('IRC Module version %s loaded.' % (ircVersion) )
