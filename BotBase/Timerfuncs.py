import imp, time
from threading import Thread, Timer
from BotBase import BotTools

tVersion = '1.0-rc1'

class timerExec(Thread):
	def __init__(self, timing, module, timerfunc, irc):
		Thread.__init__(self)
		self.timing = timing
		self.module = module
		self.funcname = timerfunc
		self.timerfunc = getattr(self.module, self.funcname)
		self.irc = irc
		self.timer = dict()
		self.reset(self.timing)
		
	def run(self):
		self.running = True
		while self.running and callable(self.timerfunc):
			self.timerfunc = getattr(self.module, self.funcname)
			for i in self.timer:
				if not self.timer[i].is_alive():
					self.timer[i] = Timer(self.getDelay(), self.timerfunc, [self.irc, i]);
					self.timer[i].start()
			time.sleep(2)
		
		for i in self.timer:
			if self.timer[i].is_alive():
				self.timer[i].cancel()
		print('timerExec shut down')

	def reset(self, timing):
		self.setTiming(timing)
		for i in self.timer:
			self.timer[i].cancel()
		self.timer = dict()
		self.channelTimers()
	
	def setTiming(self, timing):
		if timing != self.timing:
			self.timing = timing

	def channelTimers(self):
		chanlist = self.irc.getDBConn().callDBFunc('timerchannels', [self.funcname])
		for i in chanlist:
			chan = i[0]
			if chan not in self.timer:
				print('Timing for channel %s' % (chan))
				self.timer[chan] = Timer(self.getDelay(), self.timerfunc, [self.irc, chan]);
				self.timer[chan].start()

	def getDelay(self):
		if ':' in self.timing:
			min = None
			for i in self.timing.split(','):
				if min == None:
					min = i
					continue
				cur = BotTools.secondsUntilNext(i)
				if cur < BotTools.secondsUntilNext(min):
					min = i
			delay = BotTools.secondsUntilNext(min)
		else:
			delay = int(timing)
		return delay

	def stop(self):
		for chan in self.timer:
			self.timer[chan].cancel()
		self.running = False

def startTimers(irc):
	timers = irc.getConfig().getValue('timers')
	if timers == None:
		timers = dict()
	timerfuncs = irc.getDBConn().callDBFunc('getTimerFuncs')
	for i in timerfuncs:
		if i[0] in timers:
			timers[i[0]].reset(i[1])
			continue
		mod = BotTools.getModule(irc, i[0])
		timers[i[0]] = timerExec(i[1], mod, i[0], irc)
		timers[i[0]].daemon = True
		timers[i[0]].start()
	irc.getConfig().setValue('timers', timers)

def stopTimers(irc):
	timers = irc.getConfig().getValue('timers')
	if timers != None:
		for i in timers:
			timers[i].stop()
	irc.getConfig().setValue('timers', dict())

def onReload():
	imp.reload(BotTools)
	print('Timerfuncs %s loaded.' % (tVersion))
