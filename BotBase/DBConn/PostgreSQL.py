#coding=UTF-8
from BotBase.DBConn.Base import DBConn
import re, sys, traceback, psycopg2

class PostgreSQLDBConn(DBConn):
	def __init__(self, dbconfig):
		dbname = dbconfig['name']
		dbuser = dbconfig['user']
		if 'host' in dbconfig:
			dbhost = dbconfig['host']
		else:
			dbhost = 'localhost'
		connstr = "dbname='%s' user='%s' host='%s'" % (dbname, dbuser, dbhost)
		if 'pass' in dbconfig:
			connstr += " password='%s'" % (dbconfig['pass'])
		self.conn = psycopg2.connect(connstr);
		self.conn.set_client_encoding('UTF-8')
		self.connected = True

	def execute(self, query, data = list()):
		result = list()
		cur = self.conn.cursor();
		try:
			if (len(data) > 0):
				cur.execute(query, data);
			else:
				cur.execute(query);
			for i in cur:
				result.append(i)
			self.conn.commit()
		except:
			traceback.print_exc(file=sys.stdout)
			self.conn.rollback()
		cur.close()
		return result

	def callDBFunc(self, func, params = list()):
		query = 'select * from '+func+'('
		i = len(params)
		while i != 0:
			if (i != len(params)):
				query += ', '
			query += '%s'
			i -= 1
		query += ');'
		res = self.execute(query, params)
		return res

