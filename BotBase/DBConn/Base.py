#coding=UTF-8
import re, sys, traceback

class DBConn:
	def __init__(self, dbconfig):
		pass

	def addInfo(self, user, infotype, info):
		self.callDBFunc('addinfo', [user, infotype, info])

	def getInfo(self, user, infotype):
		info = self.callDBFunc("getinfo", [user, infotype])
		if (len(info) > 0):
			return info[0][0]
		return None

	def getChans(self):
		return self.execute("select name from channel;")

	def getUser(self, user):
		regexps = self.callDBFunc('getuserhosts');
		for (h, s) in regexps:
			if (re.match(s, user) or h == user):
				return h, s
		return None, None;
		
	def listUsers(self):
		return self.execute("select handle from ircuser order by id")
		
	def listFuncs(self):
		return self.callDBFunc("getfuncs")

	
	def funcAllowed(self, uinfo, chan, func):
		args = list()
		if chan != uinfo['nick']:
			args.append(chan)
		args.append(func)
		handle = uinfo['user']
		allowed = self.callDBFunc('funcallowed', args)
		for i in allowed:
			if handle == i[1] or i[1] == None:
				return True
		return False
		
	def getMod(self, func):
		result = self.callDBFunc("getfuncmod", [func]);
		return result[0][0];

	def execute(self, query, data = list()):
		# You need to implement this for your own DB
		pass

	def callDBFunc(self, dbfunc, params = list()):
		# You need to implement this for your own DB
		pass
