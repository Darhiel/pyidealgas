import sqlite3, traceback, sys
from BotBase.DBConn.Base import DBConn

class SQLiteDBConn(DBConn):
	def __init__(self, dbconfig):
		DBConn.__init__(self, dbconfig)
		if 'dbfile' not in dbconfig:
			dbconfig['dbfile'] = "idealgas.db"
		self.dbfile = dbconfig['dbfile'];
		tables = self.execute("SELECT name FROM sqlite_master where type = 'table';")
		if len(tables) == 0:
			print("If you are not doing your initial db population and you get this message, you are in trouble.")
			self.populateDB()
				
	def execute(self, query, data = list()):
		result = list()
		conn = sqlite3.connect(self.dbfile)
		cur = conn.cursor();
		try:
			if (len(data) > 0):
				cur.execute(query, data);
			else:
				cur.execute(query);
			for i in cur:
				result.append(i)
			conn.commit()
		except:
			traceback.print_exc(file=sys.stdout)
			cconn.rollback()
		cur.close()
		return result
	
	def callDBFunc(self, funcname, params = list()):
		try:
			func = getattr(self, funcname.upper())
			if callable(func):
				return func(*params)
			else:
				raise Exception("Calling a method that is not callable; %s" % (funcname.upper()))
		except:
			traceback.print_exc(file=sys.stdout)
			print('Missing method %s' % (funcname.upper()))

	## Bot Functionality. Don't touch if you don't know what it is. ##

	## Data Adding ##

	def ADDUSER(self, handle, regexp):
		if self.dataExists("userhost", ["regexp"], [regexp]):
			return []
		userid = self.getId("ircuser", "handle", handle)
		return self.execute('insert into userhost(uid, regexp) values (?, ?)', [userid, regexp])

	def ADDCHANNEL(self, channel):
		self.getId("channel", "name", channel)

	def GETMODULE(self, modulename):
		return self.getId("module", "name", modulename)

	def ADDCALLFUNCTION(self, module, funcname): 
		if self.addCommand(module, funcname):
			funcId = self.getId("command", "func", funcname)
			self.execute("insert into callfunc(id) values (?);", [funcId])
			return [(funcId,)]
		return []

	def ADDSPECIALFUNCTION(self, module, funcname, functype):
		if self.addCommand(module, funcname):
			funcId = self.getId("command", "func", funcname)
			self.execute("insert into specialfunc(id, ftype) values (?, ?);", [funcId, functype])
			return [(funcId,)]
		return []

	def ADDTEXTFUNCTION(self, module, funcname, regexp):
		if self.addCommand(module, funcname):
			funcId = self.getId("command", "func", funcname)
			self.execute("insert into textfunc(id, regexp) values (?, ?);", [funcId, regexp])
			return [(funcId,)]
		return []

	def ADDTIMERFUNCTION(self, module, funcname, timing):
		if self.addCommand(module, funcname):
			funcId = self.getId("command", "func", funcname)
			self.execute("insert into timerfunc(id, timing) values (?, ?);", [funcId, timing])
			return [(funcId,)]
		return []

	def LIMITFUNC(self, handle, funcname):
		if not self.dataExists("command", ["func"], [funcname]):
			return []
		funcId = self.getId("command", "func", funcname)
		userId = self.getId("ircuser", "handle", handle)
		if self.dataExists("limited", ["uid", "fid"], [userId, funcId]):
			return []
		return self.execute("insert into limited (uid, fid) values (?, ?)", [userId, funcId])

	def ADDINFO(self, handle, type, data):
		userId = self.getId("ircuser", "handle", handle)
		infoTypeId = self.getId("infotype", "name", type)
		self.execute("INSERT OR REPLACE info(uid, itype, content) values (?, ?, ?)", [userId, infoTypeId, data])
		return [(userId, infoTypeId)]

	## Data Retrieval ##

	def GETINFO(self, handle, type):
		userId = self.getId("ircuser", "handle", handle)
		infoTypeId = self.getId("infotype", "name", type)
		return self.execute("SELECT content from info where uid = ? and itype = ?", [userId, infoTypeId])

	def GETTIMERFUNCS(self):
		return self.execute("SELECT cmd.func, t.timing FROM command cmd, timerfunc t WHERE t.id = cmd.id")

	def GETUSERHOSTS(self):
		return self.execute("SELECT i.handle, u.regexp FROM ircuser i, userhost u WHERE i.id = u.uid")

	def GETFUNCS(self, *args):
		if len(args) == 2:
			return self.execute('''SELECT cmd.func FROM channel c, command cmd, specialfunc j, active a
				WHERE j.id = cmd.id AND a.fid = cmd.id and c.id = a.cid and c.name = ?
				AND j.ftype = ?;''', args)
		elif len(args) == 1:
			return self.execute('''SELECT cmd.func FROM command cmd, specialfunc j
				WHERE j.id = cmd.id AND j.ftype = ?''', args);
		else:
			return self.execute("select m.name, c.func from command c, module m where m.id = c.mid order by m.name, c.func")
	
	def GETTEXTFUNCS(self, channel):
		return self.execute('''SELECT cmd.func, t.regexp FROM channel c, command cmd, textfunc t, active a
			WHERE t.id = cmd.id AND a.fid = cmd.id and c.id = a.cid and c.name = ?''', [channel]);

	def FUNCALLOWED(self, *args):
		if len(args) == 2:
			return self.execute('''SELECT cmd.id, cmd.handle FROM channel c, active a,
				(SELECT l.handle, c.id FROM
					(SELECT c.id, c.func FROM command c, callfunc f WHERE c.id = f.id AND c.func = ?) c LEFT JOIN
					(SELECT l.fid, i.handle FROM ircuser i, limited l WHERE l.uid = i.id) l ON l.fid = c.id) cmd
				WHERE a.fid = cmd.id AND a.cid = c.id AND c.name = ?''', [args[1], args[0]]);
		elif len(args) == 1:
			return self.execute('''SELECT cmd.id, cmd.handle FROM (SELECT l.handle, c.id FROM
				(SELECT c.id, c.func FROM command c, callfunc f WHERE c.id = f.id AND c.func = ?) c LEFT JOIN
				(SELECT l.fid, i.handle FROM ircuser i, limited l WHERE l.uid = i.id) l ON l.fid = c.id) cmd;''', args);

	def GETFUNCMOD(self, funcname):
		return self.execute("SELECT m.name from command c, module m where c.func = ? and c.mid = m.id", [funcname]);

	def TIMERCHANNELS(self, timerfunc):
		return self.execute('''SELECT c.name from channel c, active a, timerfunc t, command cm
 			WHERE cm.func = ? and cm.id = t.id and a.fid = t.id and c.id = a.cid''', [timerfunc]);

	## Bot Functionality activation ##

	def ACTIVATE(self, channel, funcname):
		if not self.dataExists("channel", ["name"], [channel]) or not self.dataExists("command", ["func"], [funcname]):
			return []
		funcId = self.getId("command", "func", funcname)
		chanId = self.getId("channel", "name", channel)
		if self.dataExists("active", ["cid", "fid"], [chanId, funcId]):
			return []
		self.execute("INSERT INTO active (cid, fid) VALUES (?, ?)", [chanId, funcId]);
		return self.execute("SELECT cid, fid FROM active where cid = ? and fid = ?", [chanId, funcId]);

	def DEACTIVATE(self, channel, funcname):
		if not self.dataExists("channel", ["name"], [channel]) or not self.dataExists("command", ["func"], [funcname]):
			return []
		funcId = self.getId("command", "func", funcname)
		chanId = self.getId("channel", "name", channel)
		if not self.dataExists("active", ["cid", "fid"], [chanId, funcId]):
			return []
		self.execute("DELETE FROM active WHERE cid = ? and fid = ?", [chanId, funcId])
		return [(chanId, funcId)]

	## Bot Functionality Helpers ##

	def addCommand(self, module, funcname):
		moduleId = self.getmodule(module)
		if not self.dataExists("command", ["mid", "func"], [moduleId, funcname]):
			self.execute("insert into command(mid, func) values (?, ?)", [moduleId, funcname])
			return True
		return False

	def getId(self, table, type, name):
		data = self.execute('Select id from %s where %s = ?' % (table, type), [name])
		if len(data) == 0:
			self.execute("insert into %s(%s) values (?)" % (table, type), [name])
			return self.getId(table, type, name)
		else:
			return data[0][0]

	def dataExists(self, table, types, values):
		query = "select * from %s where %s = ?" % (table, types[0])
		for i in range(1, len(types)):
			query += " and %s = ?" % (types[i])
		return len(self.execute(query + ";", values)) > 0

	## INITIAL DB CREATION ##
	def populateDB(self):
		self.execute('''
CREATE TABLE ircuser (
 id integer primary key autoincrement,
 handle varchar unique not null
);''')
		self.execute('''
CREATE TABLE channel (
 id integer primary key autoincrement,
 name varchar unique not null
);''')
		self.execute('''
CREATE TABLE infotype (
 id integer primary key autoincrement,
 name varchar unique not null
);''')
		self.execute('''
CREATE TABLE info (
 uid int references ircuser(id) on delete cascade,
 itype int references infotype(id) on delete cascade,
 content varchar,
 primary key(uid, itype)
);''')
		self.execute('''
CREATE TABLE module (
 id integer primary key autoincrement,
 name varchar unique not null
);''')
		self.execute('''
CREATE TABLE command (
 id integer primary key autoincrement,
 mid int references module(id) on delete cascade,
 func varchar unique not null
);''')
		self.execute('''
CREATE TABLE callfunc (
 id int references command(id) on delete cascade,
 primary key (id)
);''')
		self.execute('''
CREATE TABLE specialfunc (
 id int references command(id) on delete cascade,
 ftype varchar not null
);''')
		self.execute('''
CREATE TABLE timerfunc (
 id int references command(id) on delete cascade,
 timing varchar not null
);''')
		self.execute('''
CREATE TABLE textfunc (
 id int references command(id) on delete cascade,
 regexp varchar not null
);''')
		self.execute('''
CREATE TABLE userhost (
 id integer primary key autoincrement,
 uid int references ircuser(id) on delete cascade,
 regexp varchar unique not null
);''')
		self.execute('''
CREATE TABLE limited (
 uid int references ircuser(id) on delete cascade,
 fid int references command(id) on delete cascade,
 primary key(uid, fid)
);''')
		self.execute('''
CREATE TABLE active (
 cid int references channel(id) on delete cascade,
 fid int references command(id) on delete cascade,
 primary key(cid, fid)
);''')
