import urllib.request, urllib.parse, html.entities, re, html.parser, traceback, sys
from datetime import datetime
# This is a collection for some basic tools to use in modules. from BotBase import BotTools

# Get valid username for either a person on IRC or from db.
def getUser(irc, username):
	uinfo = irc.getNicklist().getUserInfo(username)
	if username == None:
		user = None
	elif uinfo == None or uinfo['user'] == None:
		user, regex = irc.getDBConn().getUser(username)
	else:
		user = uinfo['user']
	return user

def titlecase(s):
	return re.sub(r"[A-Za-z]+('[A-Za-z]+)?",
		lambda mo: mo.group(0)[0].upper() + 
		mo.group(0)[1:].lower(), s)

class IRCDebug:
	def msg(self, target, text):
		print('%s -> %s' % (target, text))

# Generic tool to get title from a website. Thanks to luma !
def getTitle(url, spoof):
	try:
		pattern = re.compile("<title>(.*)</title>", re.S)
		request = urllib.request.Request(url)
		if spoof:
			request.add_header("User-Agent", "Mozilla/5.0 (X11; U; Linux i686; fi-FI; rv:1.9.2.8) Gecko/20100723 Ubuntu/10.04 (lucid) Firefox/3.6.8")
		page = urllib.request.urlopen(request)
		if page.info()["Content-Type"][0:9] != "text/html":
			return "";
		text = page.read(100000).decode()
		res = pattern.search(text)
		if res:
			return urllib.parse.unquote(html.parser.HTMLParser().unescape(re.sub('\s+', ' ', res.group(1))))
	except Exception as e:
		if spoof:
			return getTitle(url, False)
		else:
			return ""

# Calculates time to next occurrence of HH:MM
def secondsUntilNext(time):
	try:
		current = datetime.now().replace(year=1900, month=1, day=1)
		target = datetime.strptime(time, "%H:%M")
		tdiff = target - current
		return tdiff.seconds + 1
	except:
		return None


def getModule(irc, cmd):
	modname = "IdealMods."+irc.getDBConn().getMod(cmd)
	if modname not in sys.modules:
		__import__(modname)
	mod = sys.modules[modname]
	return mod

def onReload():
	pass
