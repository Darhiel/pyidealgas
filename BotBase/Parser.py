#coding=UTF-8
import imp
from BotBase import Reactor
from threading import Thread

pVersion = '1.0'

class Parse(Thread):
	def __init__(self,irc):
		Thread.__init__(self)
		self.irc = irc
		endl = '\r\n'
		self.endl = endl.encode()
	
	def run(self):
		self.running = True
		while self.running:
			try:
				recv = self.irc.recv(4096)
			except:
				recv = b''

			if recv == b'':
				self.irc.quit('Broken Connection', True)
				continue

			leave = 1
			if (recv.endswith(self.endl)):
				leave = 0
			rows = recv.split(self.endl)

			for row in rows[0:len(rows)-leave]:
				try:
					decoded = row.decode()
					row = decoded
				except UnicodeDecodeError:
					direct = str(row, 'ISO-8859-1')
					row = direct
					
				if (len(row.split()) > 1):
					if (row.split() [ 0 ] == 'PING'):
						self.irc.send ( 'PONG ' + row.split() [ 1 ] )
					else:
						reactor = Reactor.Reactor(self.irc, row)
						reactor.daemon = True
						reactor.start()
				else:
					break
			if (leave == 1):
				recv = rows[len(rows)-1]

	def stop(self):
		self.running = False
	changes = False

def onReload():
	imp.reload(Reactor)
	if callable(Reactor.onReload):
		Reactor.onReload()
	print('Parser %s loaded!' % (pVersion))

