#coding=UTF-8
# This file creates a Configuration class that you can use to store info on your ircbot

class Config:
	def __init__(self, user, nick, realname, server, port):
		self.data = dict()
		self.data['nick'] = nick
		self.data['user'] = user
		self.data['realname'] = realname
		self.data['server'] = server
		self.data['port'] = port

	def setValue(self, datatype, value):
		self.data[datatype] = value

	def getValue(self, datatype):
		if datatype not in self.data:
			return None
		return self.data[datatype]
