import time
from threading import Semaphore

class UserInfo:
	def __init__(self, userinfo):
		self.uinfo = userinfo
		
	def getInfo(self):
		return self.uinfo

	def setInfo(self, userinfo):
		self.uinfo = userinfo

class Nicklist:
	def __init__(self):
		self.chans = dict()
		self.nicks = dict()
		self.sem = Semaphore(1)

	def addNick(self, nick, chan):
		self.sem.acquire()
		if chan not in self.chans:
			self.chans[chan] = dict()
		self.chans[chan][nick] = (time.time(), None, False)
		self.sem.release()
		
	def delFromChan(self, nick, chan):
		self.sem.acquire()
		if chan in self.chans and nick in self.chans[chan]:
			del self.chans[chan][nick]
		self.sem.release()
			
	def delFromAll(self, nick):
		for i in self.chans:
			self.delFromChan(nick, i)
		self.sem.acquire()
		if nick in self.nicks:
			del self.nicks[nick]
		self.sem.release()
		
	def changeNick(self, oldnick, newnick):
		self.sem.acquire()
		for chan, users in self.chans.items():
			if oldnick in users:
				self.chans[chan][newnick] = self.chans[chan][oldnick]
				del self.chans[chan][oldnick]
		if oldnick in self.nicks:
			tmp = self.nicks[oldnick]
			self.nicks[newnick] = tmp
			del self.nicks[oldnick]
		self.sem.release()
	
	def setIdle(self, nick, chan, msg):
		self.sem.acquire()
		if nick not in self.chans[chan]:
			self.chans[chan][nick] = (time.time(), msg, False)	
		if self.chans[chan][nick][1] != msg:
			self.chans[chan][nick] = (time.time(), msg, False)
		self.sem.release()
		
	def getUserInfo(self, nick):
		self.sem.acquire()
		if nick in self.nicks:
			self.sem.release()
			return self.nicks[nick].getInfo()
		self.sem.release()
		return None
			
	def setUserInfo(self, nick, uinfo):
		self.sem.acquire()
		if nick not in self.nicks:
			self.nicks[nick] = UserInfo(uinfo)
		else:
			self.nicks[nick].setInfo(uinfo)
		self.sem.release()
			
	def getUsers(self, chan):
		self.sem.acquire()
		if chan in self.chans:
			self.sem.release()
			return self.chans[chan]
		self.sem.release()
		return None
		
	def update(self, uinfo, chan, utype, args):
		if uinfo != None:
			if uinfo != self.getUserInfo(uinfo['nick']):
				self.setUserInfo(uinfo['nick'], uinfo)

		if utype == 'PRIVMSG':
			self.setIdle(uinfo['nick'], chan, args)
		elif utype == 'JOIN':
			self.addNick(uinfo['nick'], chan)
		elif utype == 'NICK':
			self.changeNick(uinfo['nick'], args[0])
		elif utype == 'PART':
			self.delFromChan(uinfo['nick'], chan)
		elif utype == 'KICK':
			self.delFromChan(args[0], chan)
		elif utype == 'QUIT':
			self.delFromAll(uinfo['nick'])

	def getChannels(self):
		tmp = list()
		for i in self.chans:
			tmp.append(i)
		return tmp
