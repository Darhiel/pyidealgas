#coding=UTF-8
import re, imp, sys, traceback, time
from BotBase import CTCP, BotTools
from threading import Thread, Timer

ftypes = ['JOIN', 'PART', 'NICK', 'KICK', 'MODE', 'QUIT', 'NAMES']
rVersion = '0.9.9'

class Reactor(Thread):
	def __init__(self, bot, row):
		Thread.__init__(self)
		self.bot = bot
		self.prog = re.compile('^:'+self.bot.getConfig().getValue('cmdchar')+'(?P<cmd>.*)$')
		self.dbconn = self.bot.getDBConn()
		self.nicklist = self.bot.getNicklist()
		self.nick = self.bot.getConfig().getValue('nick')
		self.row = row

	def run(self):
		self.react(self.row)
			
	def react(self, row):
		try:
			fields = row.split()
			
			uhost = fields[0][1:]
			user, hostre = self.dbconn.getUser(uhost)
			nick = uhost.split('!')[0]
			uinfo = {'user' : user, 'nick' : nick, 'uhost' : uhost, 'hostre' : hostre}
			
			if (fields[ 1 ] == '001'):
				self.nick = fields[2];
				chans = self.dbconn.getChans()
				for i in chans:
					self.bot.join( i[0] )
			elif (fields[ 1 ] == '353'):
				chan = fields[4]
				names = fields[5][1:]
				for i in fields[6:]:
					names += ' ' + i
				modes = ['+', '@', '%', '~', '!', '&', '-']
				for i in names.split():
					if i[0] in modes:
						i = i[1:]
					if i == self.nick:
						continue
					self.nicklist.addNick(i, chan)
				self.runCommands('NAMES', uinfo, chan, names)
			elif (fields[ 1 ] == 'PRIVMSG'):
				chan = fields[2]
				if nick != self.nick and chan != self.nick:
					self.nicklist.update(uinfo, chan, fields[1], " ".join(fields[3:])[1:])
				result = self.prog.match(fields[3])
				if result:
					self.command(uinfo, result.group('cmd'), fields)
				elif len(fields[3]) > 1 and fields[3][1] == CTCP.ctcpchar:
					cmd = fields[3].split(CTCP.ctcpchar)[1]
					if cmd in CTCP.implemented:
						self.CTCPcommand(uinfo, cmd, fields)
					else:
						return
				else:
					self.textmatch(uinfo, fields)
			elif fields[ 1 ] in ['JOIN', 'PART', 'NICK', 'KICK', 'MODE', 'QUIT']:
				chan = fields[2]
				args = list()
				if fields[ 1 ] == 'JOIN':
					if (chan[0] == ':'):
						chan = chan[1:]
				elif fields[ 1 ] == 'NICK':
					chan = None
					if (fields[2][0] != ":"):
						newnick = fields[2]
					else:
						newnick = fields[2][1:]
					args.append(newnick)
					if (uinfo['nick'] == self.nick):
						self.bot.getConfig().setValue('nick', self.nick)
				elif fields[ 1 ] == 'KICK':
					kicked = fields[3]
					args.append(kicked)
				elif fields[ 1 ] == 'MODE':
					change = fields[3]
					targets = fields[4:]
					args.extend([change, targets])
				elif fields[ 1 ] == 'QUIT':
					chan = None				
				if (uinfo['nick'] != self.nick):
					self.nicklist.update(uinfo, chan, fields[1], args)
				self.runCommands(fields[1], uinfo, chan, args)
			elif self.bot.getConfig().getValue('debug'):
				print(row)
		except:
			print("Unhandled exception!")
			traceback.print_exc(file=sys.stdout)
				
	def runCommands(self, functype, uinfo, chan, args):
		if chan == None:
			funcargs = [functype]
		else:
			funcargs = [chan, functype]
		funcs = self.dbconn.callDBFunc('getFuncs', funcargs)
		for i in funcs:
			curmod = BotTools.getModule(self.bot, i[0])
			func = getattr(curmod, i[0])
			if callable(func) and not func(self.bot, uinfo, chan, args):
				return

	def CTCPcommand(self, uinfo, cmd, fields):
		chan = fields[2]
		args = fields[4:]
		func = getattr(CTCP, cmd)
		if callable(func):
			func(self.bot, uinfo, chan, args)

	def command(self, uinfo, cmdword, fields):
		chan = fields[2]
		if (chan == self.bot.getConfig().getValue('nick')):
			chan = uinfo['nick']
		args = fields[4:]
		if self.dbconn.funcAllowed(uinfo, chan, cmdword):
			if self.bot.getConfig().getValue('debug'):
				print('%s > %s: !%s' % (chan, uinfo['nick'], cmdword))
			curmod = BotTools.getModule(self.bot, cmdword);
			func = getattr(curmod, cmdword)
			if callable(func):
				func(self.bot, uinfo, chan, args)
	
	def textmatch(self, uinfo, fields):
		chan = fields[2]
		text = fields[3][1:]
		for i in fields[4:]:
			text += ' '+i;
		result = self.dbconn.callDBFunc('gettextfuncs', [chan])
		for i in result:
			func = i[0]
			regexp = i[1]
			res = re.findall(regexp, text, flags=re.I);
			for i in res:
				if i == '':
					continue
				args = [i];
				curmod = BotTools.getModule(self.bot, func)
				function = getattr(curmod, func)
				if callable(function):
					function(self.bot, uinfo, chan, args)

def onReload():
	imp.reload(CTCP)
	if callable(CTCP.onReload):
		CTCP.onReload()
	imp.reload(BotTools)
	if callable(BotTools.onReload):
		BotTools.onReload()
	print('Reactor %s loaded!' % (rVersion))

