name = 'pyIdealGas pV=nRT'
version = '1.0-rc1'
ctcpchar = chr(1)
implemented = ['VERSION', 'PING']

def VERSION(irc, uinfo, chan, args):
	msg = ctcpchar
	msg += 'VERSION %s %s by Teemu "Fal" Suo-Anttila' % (name, version)
	msg += ctcpchar
	irc.send('NOTICE %s :%s' % (uinfo['nick'], msg))
	
def PING(irc, uinfo, chan, args):
	msg = ctcpchar
	msg += 'PING'
	for i in args[2:]:
		msg += ' '+i
	msg += ctcpchar
	irc.send('NOTICE %s :%s' % (uinfo['nick'], msg))

def onReload():
	print('CTCP system for pyIdealGas %s loaded!' % (version))
