#coding=UTF-8
import sys, imp, traceback, BotBase.BotTools
from threading import Timer

# Reloading function for updating all modules on bot.
def reload(irc, uinfo, chan, args):
	irc.msg(chan, "Reloading.")
	mods = dir(sys.modules["IdealMods"])
	for i in mods:
		if i[0] != '_':
			imp.reload(sys.modules["IdealMods."+i])
			try:
				func = getattr(sys.modules["IdealMods."+i], 'onReload')
				if callable(func):
					func(irc)
			except:
				pass
	imp.reload(BotBase.BotTools)
	irc.reload()

# Shutdown function. You should probably limit this one..
def quit(irc, uinfo, chan, args):
	if (len(args) > 0):
		msg = args[0]
		for i in args[1:]:
			msg += ' '+i
		irc.quit(msg)
	else:
		irc.quit()

def restart(irc, uinfo, chan, args):
	irc.quit('Restarting...', True)
	
# THIS IS DANGEROUS! LIMIT!
def raw(irc, uinfo, chan, args):
	if (len(args) > 0):
		msg = args[0]
		for i in args[1:]:
			msg += ' '+i
		irc.send(msg)

# Example greeting function
# Uses chaninfo's "greet" field
def join_greet(irc, uinfo, chan, args):
	if (uinfo['user'] != None):
		greet = irc.getDBConn().getInfo(uinfo['user'], '%s.greet' % (chan))
		if greet != None:
			irc.msg(chan, "[%s] %s" % (uinfo['nick'], greet))
	return True

# regexp: spotify:(?:\S+:)+\S{22}
# Finds spotify URIs and provides http://open.spotify.com/ link for them
def text_spotify_dummy(irc, uinfo, chan, args):
	urlargs = args[0].split(':')[1:]
	baseurl = "http://open.spotify.com"
	for i in urlargs:
		baseurl += "/"+i
	irc.msg(chan, baseurl)

# chaninfo's field flags is up to you to develop usage.
# I myself am going to use it to store simple chars
# v = voice, o = op, b = ban
# and make some function to react to them
def join_setmode(irc, uinfo, chan, args):
	allowed_flags = ['b', 'v', 'o']
	if (uinfo['user'] == None):
		return True
	flags = irc.getDBConn().getInfo(uinfo['user'], '%s.flags' % (chan))
	if flags == None:
		return True
	for i in flags:
		if i not in allowed_flags:
			# Unknown flag, ignore
			continue;
		elif i == 'b':
			irc.send('KICK %s %s :You\'re banned.' % (chan, uinfo['nick']))
			irc.send('MODE %s +b %s' % (chan, uinfo['uhost']))
			t = Timer(30.0, irc.send, ['MODE %s -b %s' % (chan, uinfo['uhost'])])
			t.start()
			return False
		else:
			irc.send('MODE %s +%s %s' % (chan, i, uinfo['nick']))
	return True
	
def flags(irc, uinfo, chan, args):
	if (len(args) != 3):
		irc.msg(chan, "!flags chan user flags")
		return
	user = BotBase.BotTools.getUser(irc, args[1])
	if (user != None):
		irc.getDBConn().addInfo(user, '%s.flags' % (args[0]), args[2])
		irc.msg(chan, "Flags for %s on %s are now '%s'." % (args[1], args[0], args[2]))
	else:
		irc.msg(chan, "User %s was not found.." % (args[1]))

def greet(irc, uinfo, chan, args):
	if (len(args) < 3):
		irc.msg(chan, "!greet chan user greeting")
		return
	user = BotBase.BotTools.getUser(irc, args[1])
	if (user != None):
		if (len(args) == 1):
			greet = ""
		else:
			greet = " ".join(args[2:])
		irc.getDBConn().addInfo(user, "%s.greet" % (args[0]), greet)
		irc.msg(chan, "Greeting for %s on %s is now '%s'" % (args[1], args[0], greet))
	else:
		irc.msg(chan, "User %s was not found." % (args[1]))

def users(irc, uinfo, chan, args):
	users = irc.getDBConn().listUsers()
	ulist = list()
	for i in users:
		ulist.append(i[0])
	irc.msg(chan, ", ".join(ulist))

def addfunc(irc, uinfo, chan, args):
	if (len(args) != 2):
		irc.msg(chan, "!addfunc module function")
		return
	module = args[0]
	func = args[1]
	res = irc.getDBConn().callDBFunc("addcallfunction", [module, func])
	if (len(res) > 0):
		irc.msg(chan, "Added function !%s (%s)" % (func, module))
	else:
		irc.msg(chan, "Failed to add function.")
		
def adduser(irc, uinfo, chan, args):
	if (len(args) != 2):
		irc.msg(chan, "!adduser user hostregexp")
		return
	user = args[0]
	regexp = args[1]
	res = irc.getDBConn().callDBFunc("adduser", [user, regexp])
	if (len(res) > 0):
		irc.msg(chan, "Added user %s with regexp %s" % (user, regexp))
	else:
		irc.msg(chan, "Failed to add user.")

def allowfunc(irc, uinfo, chan, args):
	if (len(args) != 2):
		irc.msg(chan, "!allowfunc user function")
		return
	user = args[0]
	func = args[1]
	res = irc.getDBConn().callDBFunc("limitfunc", [user, func])
	if (len(res) > 0):
		irc.msg(chan, "Allowed function !%s to user %s" % (func, user))
	else:
		irc.msg(chan, "Failed to allow function.")

def activate(irc, uinfo, chan, args):
	if (len(args) != 2):
		irc.msg(chan, "!activate channel function")
		return
	channel = args[0]
	func = args[1]
	res = irc.getDBConn().callDBFunc("activate", [channel, func])
	if (len(res) > 0):
		irc.msg(chan, "Function !%s is now active on %s" % (func, channel))
	else:
		irc.msg(chan, "Activation failed.")
		
def addspecfunc(irc, uinfo, chan, args):
	if (len(args) != 3):
		irc.msg(chan, "!addspecfunc module function ftype")
		return
	module = args[0]
	func = args[1]
	ftype = args[2]
	if (ftype not in Reactor.ftypes):
		irc.msg(chan, "Select ftype from %s" % (", ".join(Reactor.ftypes)))
		return
	res = irc.getDBConn().callDBFunc("addspecialfunction", [module, func, ftype])
	if (res[0] != None):
		irc.msg(chan, "Added special %s-function %s (%s)" % (ftype, func, module))
	else:
		irc.msg(chan, "Failed to add special function.")

def addtextfunc(irc, uinfo, chan, args):
	if (len(args) != 3):
		irc.msg(chan, "!addtextfunc module function regexp")
		return
	module = args[0]
	func = args[1]
	regex = args[2]
	res = irc.getDBConn().callDBFunc("addtextfunction", [module, func, regex])
	if (res[0] != None):
		irc.msg(chan, "Added textmatcher %s (%s)" % (func, module))
	else:
		irc.msg(chan, "Failed to add textmatcher.")

def addtimerfunc(irc, uinfo, chan, args):
	if (len(args) != 3):
		irc.msg(chan, "!addtimerfunc module function delay")
		return
	module = args[0]
	func = args[1]
	timing = args[2]
	if ':' in timing:
		times = list()
		for i in timing.split(','):
			if BotBase.BotTools.secondsUntilNext(i) != None:
				times.append(i)
		if len(times) == 0:
			irc.msg(chan, "Bad argument.")
			return
		timings = ",".join(times)
	else:
		try:
			int(timing)
			timings = timing
		except:
			irc.msg(chan, "Bad argument.")
			return

	res = irc.getDBConn().callDBFunc("addtimerfunction", [module, func, timings])
	if (res[0] != None):
		irc.msg(chan, "Added timer %s (%s) with timing %s" % (func, module, timings))
		startTimers(irc)
	else:
		irc.msg(chan, "Failed to add timer.")
		
def deactivate(irc, uinfo, chan, args):
	if (len(args) != 2):
		irc.msg(chan, "!deactivate channel function")
		return
	channel = args[0]
	func = args[1]
	res = irc.getDBConn().callDBFunc("deactivate", [channel, func])
	if (len(res) > 0):
		irc.msg(chan, "Function %s deactivated on %s" % (func, channel))
	else:
		irc.msg(chan, "Deactivation failed.")

def functions(irc, uinfo, chan, args):
	flist = irc.getDBConn().listFuncs()
	cur = None
	funcs = list()
	for (m, f) in flist:
		if (m != cur):
			if (cur != None):
				irc.msg(chan, "%s: %s" % (cur, ", ".join(funcs)))
			cur = m
			funcs = list()
		funcs.append(f)
	if (cur != None):
		irc.msg(chan, "%s: %s" % (cur, ", ".join(funcs)))

def addchannel(irc, uinfo, chan, args):
	newchan = args[0]
	if newchan[0] != '#':
		irc.msg(chan, "Kanavaa %s ei lisätty." % (newchan))
	else:
		irc.send("JOIN %s" % (newchan))
		irc.getDBConn().callDBFunc('addchannel', [newchan])
		irc.msg(chan, "Kanava lisätty")

def addCallFuncs(irc, mod, funcs):
	for i in funcs:
		irc.getDBConn().callDBFunc("addcallfunction", [mod, i])
		if (funcs[i]):
			admins = irc.getConfig().getValue('admin')
			for user in admins:
				irc.getDBConn().callDBFunc("limitfunc", [user, i])

def addSpecFuncs(irc, mod, specs):
        for i in specs:
                if i not in Reactor.ftypes:
                        continue
                for j in specs[i]:
                        irc.getDBConn().callDBFunc("addspecialfunction", [mod, j, i])

def addTextMatchers(irc, mod, matchers):
        for i in matchers:
                for j in matchers[i]:
                        irc.getDBConn().callDBFunc("addtextfunction", [mod, i, j])

def addTimerFuncs(irc, mod, timers):
	for i in timers:
		if ':' in timers[i]:
			times = list()
			for t in timers[i].split(','):
				if BotBase.BotTools.secondsUntilNext(t) != None:
					times.append(t)
			if len(times) == 0:
				continue
			timings = ",".join(times)
		else:
			try:
				int(timers[i])
				timings = timers[i]
			except:
				continue
		irc.getDBConn().callDBFunc("addtimerfunction", [mod, i, timings])

def addmodule(irc, uinfo, chan, args):
	if uinfo['user'] != "" and len(args) == 1:
		i = args[0]
		print("Importing %s.." % (i))
		__import__("IdealMods."+i)
		mod = sys.modules["IdealMods."+i]
		try:
			func = getattr(mod, 'getCalls')
			if callable(func):
				calls = func()
				addCallFuncs(irc, i, calls)
				irc.msg(chan, 'Added %d call functions.' % (len(calls)))
		except:
       	                pass
		try:
			func = getattr(mod, 'getSpecials')
			if callable(func):
				specs = func()
				addSpecFuncs(irc, i, specs)
				irc.msg(chan, 'Added %d special functions.' % (len(specs)))
		except:
			pass
		try:
			func = getattr(mod, 'textMatchers')
			if callable(func):
				textmatch = func()
				addTextMatchers(irc, i, textmatch)
				irc.msg(chan, 'Added %d text mathcers.' % (len(textmatch)))
		except:
			pass
		try:
			func = getattr(mod, 'getTimers')
			if (callable(func)):
				timers = func()
				addTimerFuncs(irc, i, timers)
				irc.msg(chan, 'Added %d timer functions.' % (len(timers)))
				startTimers(irc)
		except:
			pass					

def config(irc, uinfo, chan, args):
	irc.getConfig().setValue(args[0], eval(args[1]))

def getCalls():
	funcs = {'reload' : True, 'quit' : True, 'raw' : True, 'restart' : True,
		'flags' : True, 'greet' : True, 'users' : True,
		'addfunc' : True, 'adduser' : True, 'allowfunc' : True, 
		'activate' : True, 'addspecfunc' : True, 
		'addtextfunc' : True, 'addtimerfunc' : True, 
		'deactivate' : True, 'functions' : True, 
		'addchannel' : True, 'addmodule' : True,
		'config' : True}
	return funcs

def getSpecials():
	funcs = { 'JOIN' : ['join_greet', 'join_setmode'] }
	return funcs

def textMatchers():
	funcs = { 'text_spotify_dummy' : ['spotify:(?:\S+:)+\S{22}'] }
	return funcs
