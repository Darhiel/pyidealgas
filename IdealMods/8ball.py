#!/usr/bin/env python3

# This is a simple example on how to make a module.
#
# Module contains: !8ball

import random

def eightball(irc, uinfo, chan, args):
    ans = ['It is certain','It is decidedly so','Without a doubt','Yes – definitely','You may rely on it','As I see it, yes','Most likely','Outlook good','Signs point to yes','Yes','Reply hazy, try again','Ask again later','Better not tell you now','Cannot predict now','Concentrate and ask again','Don\'t count on it','My reply is no','My sources say no','Outlook not so good','Very doubtful']
    irc.msg(chan, random.choice(ans))

def getCalls():
    return { '8ball' : False}
