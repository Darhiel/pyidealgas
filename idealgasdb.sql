CREATE TABLE ircuser (
   id serial primary key,
   handle varchar unique not null
);

CREATE TABLE channel (
   id serial primary key,
   name varchar unique not null
);

CREATE TABLE infotype (
   id serial primary key,
   name varchar unique not null
);

CREATE TABLE info (
   uid int references ircuser(id) on delete cascade,
   itype int references infotype(id) on delete cascade,
   content varchar,
   primary key(uid, itype)
);

CREATE TABLE module (
   id serial primary key,
   name varchar unique not null
);

CREATE TABLE command (
   id serial primary key,
   mid int references module(id) on delete cascade,
   func varchar unique not null
);

CREATE TABLE callfunc (
   id int references command(id) on delete cascade,
   primary key (id)
);

CREATE TABLE specialfunc (
   id int references command(id) on delete cascade,
   ftype varchar not null
);

CREATE TABLE timerfunc (
   id int references command(id) on delete cascade,
   timing varchar not null
);

CREATE TABLE textfunc (
   id int references command(id) on delete cascade,
   regexp varchar not null
);

CREATE TABLE userhost (
   id serial primary key,
   uid int references ircuser(id) on delete cascade,
   regexp varchar unique not null
);

CREATE TABLE limited (
   uid int references ircuser(id) on delete cascade,
   fid int references command(id) on delete cascade,
   primary key(uid, fid)
);

CREATE TABLE active (
   cid int references channel(id) on delete cascade,
   fid int references command(id) on delete cascade,
   primary key(cid, fid)
);

CREATE OR REPLACE FUNCTION getmodule(modname varchar) returns integer
   LANGUAGE plpgsql
   AS $$
      DECLARE modid int;
   BEGIN
      SELECT into modid id from module where name = modname;
      IF modid IS NULL THEN
         INSERT INTO module (name) values (modname) returning id into modid;
      END IF;
      RETURN modid;
   END;
$$;

CREATE OR REPLACE FUNCTION addcallfunction(modname character varying, funcname character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
        DECLARE 
                func_id int;
                mod_id int;
        BEGIN
                SELECT into mod_id getmodule from getmodule(modname);
                INSERT INTO command (mid, func) values (mod_id, funcname) returning id into func_id;
                INSERT INTO callfunc (id) values (func_id);
                RETURN func_id;
        END;
$$;

CREATE OR REPLACE FUNCTION addinfo(hand character varying, infot character varying, cont character varying) 
RETURNS TABLE(userid integer, typeid integer)
    LANGUAGE plpgsql
    AS $$
        DECLARE 
                itid int;
                usid int;
        BEGIN
                SELECT INTO usid id FROM ircuser WHERE handle = hand;
                IF usid IS NULL THEN
                        RETURN QUERY SELECT NULL, NULL;
                ELSE
                        SELECT INTO itid id FROM infotype WHERE name = infot;
                        IF itid IS NULL THEN
                                INSERT INTO infotype (name) values (infot) RETURNING id INTO itid;
                        END IF;
                        
                        IF (SELECT content FROM info WHERE uid = usid AND itype = itid) IS NOT NULL THEN
                                UPDATE info SET content = cont WHERE uid = usid and itype = itid;
                        ELSE
                                INSERT INTO info (uid, itype, content) values (usid, itid, cont);
                        END IF;
                        RETURN QUERY SELECT usid, itid;
                END IF;
        END;
$$;

CREATE OR REPLACE FUNCTION adduser(hand character varying, regex character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
        DECLARE userid int;
        BEGIN
                SELECT into userid id FROM ircuser WHERE handle = hand;

                IF userid IS NULL THEN
                        INSERT INTO ircuser (handle) values ($1);
                        SELECT into userid id FROM ircuser WHERE handle = hand;
                END IF;

		IF (SELECT id from userhost where regexp = regex) IS NULL THEN
                        INSERT INTO userhost (uid, regexp) VALUES (userid, regex);
                END IF;
                RETURN userid;
        END;
$$;

CREATE OR REPLACE FUNCTION funcallowed(channel character varying, funcname character varying) RETURNS TABLE(id int, handle character varying)
    LANGUAGE plpgsql
    AS $$
        BEGIN
            RETURN QUERY 
                SELECT cmd.id, cmd.handle FROM channel c, active a, 
                    (SELECT l.handle, c.id FROM 
                        (SELECT c.id, c.func FROM command c, callfunc f WHERE c.id = f.id AND c.func = funcname) c LEFT JOIN 
                        (SELECT l.fid, i.handle FROM ircuser i, limited l WHERE l.uid = i.id) l ON l.fid = c.id) cmd 
                WHERE a.fid = cmd.id AND a.cid = c.id AND c.name = channel;
        END;
$$;

CREATE OR REPLACE FUNCTION funcallowed(funcname character varying) RETURNS TABLE(id int, handle character varying)
    LANGUAGE plpgsql
    AS $$
        BEGIN
            RETURN QUERY 
                SELECT cmd.id, cmd.handle FROM (SELECT l.handle, c.id FROM 
                        (SELECT c.id, c.func FROM command c, callfunc f WHERE c.id = f.id AND c.func = funcname) c LEFT JOIN 
                        (SELECT l.fid, i.handle FROM ircuser i, limited l WHERE l.uid = i.id) l ON l.fid = c.id) cmd;
        END;
$$;

CREATE OR REPLACE FUNCTION getinfo(hand character varying, infot character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
        BEGIN
                RETURN (SELECT i.content FROM infotype it, info i, ircuser iu
                         WHERE iu.id = i.uid and it.id = i.itype and iu.handle = $1
                         and it.name = $2);
        END;
$$;

CREATE OR REPLACE FUNCTION limitfunc(username character varying, funcname character varying) RETURNS TABLE(userid integer, funcid integer)
    LANGUAGE plpgsql
    AS $$
        DECLARE 
                usid int;
                fuid int;
        BEGIN
                SELECT INTO usid id FROM ircuser WHERE handle = username;
                SELECT INTO fuid id FROM command WHERE func = funcname;
                
                IF usid IS NOT NULL AND fuid IS NOT NULL THEN
                        IF (SELECT uid FROM limited WHERE uid = usid and fid = fuid) IS NULL THEN
                                INSERT INTO limited (uid, fid) VALUES (usid, fuid);
                        END IF;
                        RETURN QUERY (SELECT * FROM limited where uid = usid and fid = fuid);
                END IF;
        END;
$$;

CREATE OR REPLACE FUNCTION getFuncs(chan varchar, ftname varchar) RETURNS TABLE(func varchar)
    LANGUAGE plpgsql
    AS $$
        BEGIN
                RETURN QUERY
                        SELECT cmd.func FROM channel c, command cmd, specialfunc j, active a
                        WHERE j.id = cmd.id AND a.fid = cmd.id and c.id = a.cid and c.name = chan
                        AND j.ftype = ftname;
        END;
$$;

CREATE OR REPLACE FUNCTION getFuncs(ftname varchar) RETURNS TABLE(func varchar)
    LANGUAGE plpgsql
    AS $$
        BEGIN
                RETURN QUERY
                        SELECT cmd.func FROM command cmd, specialfunc j
                        WHERE j.id = cmd.id AND j.ftype = ftname;
        END;
$$;

CREATE OR REPLACE FUNCTION getTimerFuncs() RETURNS TABLE(func varchar, delay varchar)
    LANGUAGE plpgsql
    AS $$
        BEGIN
                RETURN QUERY
                        SELECT cmd.func, t.timing FROM command cmd, timerfunc t
                        WHERE t.id = cmd.id;
        END;
$$;


CREATE OR REPLACE FUNCTION getTextfuncs(chan varchar) RETURNS TABLE(func varchar, regexp varchar)
    LANGUAGE plpgsql
    AS $$
        BEGIN
                RETURN QUERY
                        SELECT cmd.func, t.regexp FROM channel c, command cmd, textfunc t, active a
                        WHERE t.id = cmd.id AND a.fid = cmd.id and c.id = a.cid and c.name = chan;
        END;
$$;

CREATE OR REPLACE FUNCTION getuserhosts() RETURNS TABLE(hand varchar, regex varchar)
   LANGUAGE plpgsql
   AS $$
      BEGIN
         RETURN QUERY
            SELECT i.handle, u.regexp FROM ircuser i, userhost u
            WHERE i.id = u.uid;
      END;
$$;

CREATE OR REPLACE FUNCTION activate(chan varchar, fnc varchar) RETURNS TABLE(ch int, fn int)
   LANGUAGE plpgsql
   AS $$
      DECLARE
         chid int;
         fnid int;
      BEGIN
         SELECT into fnid id FROM command where func = fnc;
         SELECT into chid id FROM channel where name = chan;

         IF fnid IS NOT NULL AND chid IS NOT NULL THEN
            IF (SELECT cid FROM active WHERE cid = chid and fid = fnid) IS NULL THEN
               INSERT INTO active (cid, fid) VALUES (chid, fnid);
            END IF;
            RETURN QUERY (SELECT cid, fid FROM active where cid = chid and fid = fnid);
         END IF;
      END;
$$;

CREATE OR REPLACE FUNCTION deactivate(chan varchar, fnc varchar) RETURNS TABLE(ch int, fn int)
   LANGUAGE plpgsql
   AS $$
      DECLARE
         chid int;
         fnid int;
      BEGIN
         SELECT into fnid id FROM command where func = fnc;
         SELECT into chid id FROM channel where name = chan;

         IF fnid IS NOT NULL AND chid IS NOT NULL THEN
            IF (SELECT cid FROM active WHERE cid = chid and fid = fnid) IS NOT NULL THEN
               DELETE FROM active WHERE cid = chid and fid = fnid;
               RETURN QUERY SELECT chid, fnid;
            END IF;
         END IF;
      END;
$$;


CREATE OR REPLACE FUNCTION addtextfunction(mod varchar, fnc varchar, regex varchar) RETURNS INTEGER
   LANGUAGE plpgsql
   AS $$
      DECLARE
         fnid int;
         modid int;
      BEGIN
         SELECT into fnid id FROM command where func = fnc;
         SELECT into modid getmodule FROM getmodule(mod);

         IF fnid IS NULL THEN
            INSERT INTO command (mid, func) VALUES (modid, fnc) returning id into fnid;
            IF fnid IS NOT NULL THEN
               INSERT INTO textfunc (id, regexp) values (fnid, regex);
               RETURN fnid;
            END IF;
         END IF;
         RETURN NULL;
      END;
$$;

CREATE OR REPLACE FUNCTION addspecialfunction(mod varchar, fnc varchar, ft varchar) RETURNS INTEGER
   LANGUAGE plpgsql
   AS $$
      DECLARE
         fnid int;
         modid int;
      BEGIN
         SELECT into fnid id FROM command where func = fnc;
         SELECT into modid getmodule FROM getmodule(mod);

         IF fnid IS NULL THEN
            INSERT INTO command (mid, func) VALUES (modid, fnc) returning id into fnid;
            IF fnid IS NOT NULL THEN
               INSERT INTO specialfunc (id, ftype) values (fnid, ft);
               RETURN fnid;
            END IF;
         END IF;
         RETURN NULL;
      END;
$$;

CREATE OR REPLACE FUNCTION addtimerfunction(mod varchar, fnc varchar, del varchar) RETURNS INTEGER
   LANGUAGE plpgsql
   AS $$
      DECLARE
         fnid int;
         modid int;
      BEGIN
         SELECT into fnid id FROM command where func = fnc;
         SELECT into modid getmodule from getmodule(mod);

         IF fnid IS NULL THEN
            INSERT INTO command (mid, func) VALUES (modid, fnc) returning id into fnid;
            IF fnid IS NOT NULL THEN
               INSERT INTO timerfunc (id, timing) values (fnid, del);
               RETURN fnid;
            END IF;
         END IF;
         RETURN NULL;
      END;
$$;

CREATE OR REPLACE FUNCTION getfuncmod(fnc varchar) returns varchar
   LANGUAGE plpgsql
   AS $$
      DECLARE mod varchar;
   BEGIN
      SELECT into mod m.name from command c, module m where c.func = fnc and c.mid = m.id;
      RETURN mod;
   END;
$$;

CREATE OR REPLACE FUNCTION getfuncs() returns TABLE(mod varchar, fnc varchar)
   LANGUAGE plpgsql
   AS $$
   BEGIN
      RETURN QUERY (select m.name, c.func from command c, module m where m.id = c.mid order by m.name, c.func);
   END;
$$;

CREATE OR REPLACE FUNCTION addchannel(chan varchar) returns integer
   LANGUAGE plpgsql
   AS $$
   DECLARE
      chan_id int;
   BEGIN
      select into chan_id id from channel where name = chan;
      if chan_id IS NULL THEN
         INSERT INTO CHANNEL (name) values (chan) returning id into chan_id;
      END IF;
      RETURN chan_id;
   END;
$$;

create or replace function timerchannels(tfunc varchar) returns table (channel varchar)
   LANGUAGE plpgsql
   AS $$
   BEGIN
      RETURN QUERY
         SELECT c.name from channel c, active a, timerfunc t, command cm 
         WHERE cm.func = tfunc and cm.id = t.id and a.fid = t.id and c.id = a.cid;
   END;
$$;
