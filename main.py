#coding=UTF-8
import random
from BotBase import IRC, Config
# To use another dbconn class just import as DB
from BotBase.DBConn.SQLite import SQLiteDBConn as DB

# Adminusers to be created
# {'nick' : 'uhostrexpep', 'nick2' : 'anotheruhost'}
admin = {'AdminNick' : 'Admin!adminuser@admin\.domain\.net'}
# Used by dbpopul.py. Automatically creates a db with listed modules
# Modules can be added later via /msg Botnick !addmodule module
modules = ['bot-basic', 'myawesummodule']
# SQLite DB needs only a filename. If you are using another DB, 
# it might need more setup.
dbconfig = {'dbfile' : 'idealgas.db'}

# IRC connection info
server = "ircserver";
port = 6667;
nick = "Radon"
realname = "Not-So-Idealgas"
user = "radon"
cmdchar = '!'
# Important flooding and reconnecting related values
# delay = time between sending messages
# reconnect = time between reconnection attempts
# timeout = socket timeout
delay = 1
reconnect = 45
timeout = 350
operpass = None

# Starting from here, there is no longer setup. You can stop here.

conf = Config.Config(user, nick, realname, server, port)
conf.setValue('cmdchar', cmdchar)
# IRC send delay
conf.setValue('delay', delay)
conf.setValue('reconnect', reconnect)
conf.setValue('timeout', timeout)
conf.setValue('admin', admin)
conf.setValue('dbconfig', dbconfig)
conf.setValue('dbconn', DB)
# Debugging is disabled
conf.setValue('debug', False)
conf.setValue('operpass', operpass)

# We don't start  the bot for dbpopul.
if __name__ == '__main__': 
	# Make sure random is properly seeded
	random.seed
	# Start IRCBot, give it the config so it knows what to do..
	IRC.IRCBot(conf)
